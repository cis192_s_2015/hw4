import unittest
from hw4 import *


class TestHomework4(unittest.TestCase):

    def setUp(self):
        pass

    # memo
    def test_memo(self):

        @memo
        def my_add(x, y):
            return x + y

        self.assertEqual(my_add(1, 2), (3, False))
        self.assertEqual(my_add(1, 2), (3, True))

    # register
    def test_register(self):
        @register('+')
        def my_plus(a, b):
            return a + b

        @register('-')
        def my_sub(a, b):
            return a - b

        self.assertEqual(dispatch['+'](1, 2), 3)
        self.assertEqual(dispatch['-'](1, 2), -1)
        self.assertEqual(my_sub(3, 2), 1)
        self.assertEqual(my_plus(4, 2), 6)


    # Rational
    def test_rational(self):
        neg_2 = Rational(10, -5)
        half = Rational(1, 2)
        two = Rational(2, 1)
        quart = Rational(1, 4)
        two_3rd = Rational(2, 3)
        self.assertEqual(str(neg_2), '-2')
        self.assertEqual(half - two, Rational(-3, 2))
        self.assertEqual(neg_2 / half, Rational(-4, 1))
        self.assertTrue(quart < two_3rd)
        self.assertFalse(quart >= two_3rd)

    # Node
    def test_node(self):
        tree = Node(5)
        self.assertEqual(str(tree), 'Node(5)')
        tree.insert(6)
        self.assertEqual(str(tree), 'Node(None, 5, Node(6))')
        tree.insert(1)
        self.assertEqual(str(tree), 'Node(Node(1), 5, Node(6))')
        tree.insert(4)
        self.assertEqual(str(tree), 'Node(Node(None, 1, Node(4)), 5, Node(6))')
        self.assertEqual(tree.elements(), [1, 4, 5, 6])


def main():
    unittest.main()

if __name__ == '__main__':
    main()
